/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libros;

import java.util.Scanner;

/**
 *
 * @author caelsa
 */
public class Libros {
String autor;
String editorial;
String lugardepublicacion;
String fechadepublicacion;
String titulopublicacion; 
int isbn;
boolean r= false;
    public Libros(String autor, String titulopublicacion) {
        this.autor = autor;
        this.titulopublicacion = titulopublicacion;
    }

    public Libros(String autor, String editorial, String lugardepublicacion, String fechadepublicacion, String titulopublicacion, int isbn) {
        this.autor = autor;
        this.editorial = editorial;
        this.lugardepublicacion = lugardepublicacion;
        this.fechadepublicacion = fechadepublicacion;
        this.titulopublicacion = titulopublicacion;
        this.isbn = isbn;
    }

    



public String getAutor() {
        return autor;
        // TODO code application logic here
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getLugardepublicacion() {
        return lugardepublicacion;
    }

    public void setLugardepublicacion(String lugardepublicacion) {
        this.lugardepublicacion = lugardepublicacion;
    }

    public String getFechadepublicacion() {
        return fechadepublicacion;
    }

    //tipoFun nombreFun( string  ){}
    public void setFechadepublicacion(String fechadepublicacion) {
        this.fechadepublicacion = fechadepublicacion;
    }

    public int getIsbn() {
        return isbn;
    }

    public String getTitulopublicacion() {
        return titulopublicacion;
    }

    public void setTitulopublicacion(String titulopublicacion) {
        this.titulopublicacion = titulopublicacion;
    }

    
    /**carecteristicas del objeto libros
     *
     *
     * @param args the command line arguments
     */
    public void setIsbn(int isbn) {    
        this.isbn = isbn;
    }

    public static void main(String[] args)
    {
        Libros uno, dos;
        String autor, titulo,editorial,lugar,fecha,isbn;
        Scanner sc = new Scanner(System.in);
        
        System.out.println("    Ingrese nombre");
        autor = sc.nextLine();
        System.out.println("    Ingrese una editorial");
        editorial = sc.nextLine();
        System.out.println("    lugar de publicacion");
        lugar = sc.nextLine();
        System.out.println("    fecha de publicacion");
        fecha = sc.nextLine();
        System.out.println("    Ingrese titulo de publicacion");
        titulo = sc.nextLine();
        System.out.println("    Ingrese el isbn");
        isbn =sc.nextLine();
        
        
        
        uno = new Libros (autor , titulo);
    
        dos = uno.copiar();
        
        if(uno.comparar(dos))
            System.out.println(" Los libros som iguales\n"+uno.imprimir()+ "\n "+dos.imprimir());
        else
            System.out.println(" Los libros  NO son iguales\n"+uno.imprimir()+ "\n "+dos.imprimir());
        
        dos.setAutor("Ronaldo");
        
        if(uno.comparar(dos))
            System.out.println(" Los libros  son iguales\n"+uno.imprimir()+ "\n "+dos.imprimir());
        else
            System.out.println(" Los libros  NO son iguales\n"+uno.imprimir()+ "\n "+dos.imprimir());
        
        
        
    }
    
    boolean comparar (Libros l) 
    {
        boolean r= false;
        
        if(this.autor != l.getAutor())
            r = false;
        else if (this.editorial!= l.getEditorial())
            r = false;
        else if (this.lugardepublicacion!= l.getLugardepublicacion())
                r= false;
        else if (this.fechadepublicacion!= l.getFechadepublicacion())
            r= false;
        else if (this.titulopublicacion != l.getTitulopublicacion())
             r = false;
        else if (this.isbn!= l.getIsbn())
        
            r = true;
               
        return r;
        
    }
    
    String imprimir()
    {
        String r = "";
        r+=" Nombre "+ this.autor ;
        r+="editorial"+ this.editorial;
        r+="lugar de publicacion"+ this.lugardepublicacion;
        r+="fecha de publicacion"+ this.fechadepublicacion;
        r+= " Titulo "+this.titulopublicacion;
        return r;
    }
    
    Libros copiar()
    {
        Libros nuevo = new Libros(this.autor, this.titulopublicacion);
        return nuevo;
    }
}
